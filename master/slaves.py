#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.


from buildbot.buildslave import BuildSlave

# FIXME:  To hook new slaves, add BuildSlave objects to the slaves list.
#	BuildSlave takes at least 2 arguments: SlaveName and SlavePassword
#	The SlaveName must be the same as provided when creating the slave.
#	This is used for the slave to authenticate with the master.

slaves = []
slaves.append(BuildSlave("VL7-STD-GOLD","vl7-password"))

