#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

""" builders.py
Generates Builder objects for use with master.cfg based on 
the text file named 'manifest'
"""
import textwrap
from buildbot.steps.shell import ShellCommand
from buildbot.steps.source import Git
from buildbot.process.factory import BuildFactory
from buildbot.config import BuilderConfig
import os

builders = []

class Manifest(object):
	def __init__(self, path='manifest'):
		self.path = path
		assert os.path.exists(self.path), "manifest file does not exist"
		self.entries = self.get_entries()

	def is_entry(self, line):
		'''Test the provided line to make sure it contains
		valid text. '''
		if line.startswith("#") or line.strip() == "":
			return False
		return True

	def get_entries(self):
		''' open the file and read the entries out of it '''
		lst = []
		f = open(self.path, 'r')
		data = f.readlines()
		f.close()
		for line in data:
			if self.is_entry(line):
				lst.append(line.strip().replace("\n",""))
		return lst

def generate_builders():
	""" come up with the builder objects """
	bbdir="/tmp/buildbot/vectorlinux"
	slaves=["VL7-STD-GOLD"]
	# FIXME:  FIXME: ^^ The slave names should come from slaves.py instead of hardcoded
	manifest = Manifest()
	for slave in slaves:
		for entry in manifest.entries:
			factory = BuildFactory()
			factory.addStep(Git(repourl='https://bitbucket.org/m0e_lnx/vl_slackbuilds.git',
				mode='copy'))
			factory.addStep(ShellCommand(
				command=['sh','%s.SlackBuild'% entry],
				workdir=os.path.join(bbdir, slave, entry, "build", entry, "src")))
			bldr = BuilderConfig(name = entry,
					slavenames = slaves,
					properties={"appname": entry},
					factory = factory)
			builders.append(bldr)

# call generate builders
generate_builders()



