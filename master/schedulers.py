#!/usr/bin/env python

#    This file is part of vlbuildbot.
#
#    vlbuildbot is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    vlbuildbot is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.


from buildbot.schedulers.basic import SingleBranchScheduler
from buildbot.changes import filter
import builders
import re
schedulers = []

# make a scheduler for each builder.
# This allows us to build only the changed applications rather than the entire repo.

def is_changed(pattern):
	def check_commit(change):
		for line in change.files:
			if line.startswith(pattern): 
				return True
		return False
	return check_commit

for bldr in builders.builders:
	aname = bldr.properties['appname']
	_filter = filter.ChangeFilter(filter_fn=is_changed(aname))
	sch = SingleBranchScheduler(
			name=bldr.name,
			change_filter=_filter,
			treeStableTimer=None,
			builderNames = [ bldr.name ])
	schedulers.append(sch)

