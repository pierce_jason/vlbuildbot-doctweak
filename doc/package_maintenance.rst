================================
PACKAGE MAINTENANCE GUILDELINES 
================================
This guide explains how package maintenance is done for packages contributed to the `VectorLinux Buildbot 
<http://vlcore.vectorlinux.com/buildbot>`_ system.  The following guidelines apply to all package maintenance performed to every 
package in the database.

General Guidelines
==================
* All package revisions **MUST** increase the ``BUILD`` value on the updated package.  This will help ``slapt-get`` detect the 
available update on all end-user's machines.
* All version bumps (increases) **MUST** reset the ``BUILD`` value back to 1
* All SlackBuilds must be tested by the contributor before they are submitted.

How to maintain a package
-------------------------
When performing maintenance on a package, always follow these guidelines to make sure your contributions are in line with the 
rest of the development efforts.

* Update your working copy of the slackbuilds repository.  If you do not have one, check out a new one.  The repository can be 
accessed at `http://bitbucket.org/vlcore/vabs <http://vlcore.vectorlinux.com/buildbot>`_.  The build SlackBuilds can be found in 
the var/vabs directory of that repo.
* Navigate to the var/vabs directory in your local copy: ``cd vabs/var/vabs``
* Locate the application(s) you want to maintain or update, and open the SlackBuild with your favorite text editor.
* Make your changes to the build script and save it (with the same name).  Changes should include any changes in package version, 
build number, URL to source tarball **direct download**, or other necessary build procedure changes.
* Run the script to test it's basic functionality.
* When you have verified that the script runs and does not return an error, then you commit your changes, and push them to the 
online git repository.

Typical maintenance example
----------------------------
A typical package maintenance procedure would be necessary when a new version of an application is released from upstream.  At 
that time, the packager would need to do the following.

* Find the SlackBuild in the repository that builds the application that was just released.
* Edit the SlackBuild to update its ``VERSION`` value
* Reset the ``BUILD`` value  back to 1 (with every version bump, the release should be reset back to 1)
* Make sure the ``LINK`` value can download the new version of the source tarball.
* Commit and push changes.

FAQ
===
Q:  
    If I have to test the SlackBuild myself, why can't I just upload package instead of the SlackBuild?

A:  
    Contributing a SlackBuild results in multiple packages for different architectures created from your SlackBuild.  Uploading a 
package only contributes to **your** current arquitecture.

 


